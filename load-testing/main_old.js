import { check, group, sleep } from "k6";
import http from "k6/http";

// Test configuration
export let options = {
    // rampup for 15s 1 to 25, stay at 25, and then down to 0
    stages: [
        {duration: "5s", target: 25},
        {duration: "5s", target: 25},
        {duration: "5s", target: 0}
    ],
    thresholds: {
        "http_req_duration": ["p(95)<180"]
    },
    ext: {
        loadimpacts: {
            name: "test.loadimpact.com"
        }
    }
}

// user scenario
export default function() {
    
    group("api/metrics/octocat/Hello-World/code-review-involvement", function(){
        let res = http.get("http://a840213db1fa1454fadad370e4494e72-144729119.us-east-1.elb.amazonaws.com/api/metrics/octocat/Hello-World/code-review-involvement");
        check(res, {
            "is status 200": (r) => r.status === 200
        });

        sleep(5);
    });

}
