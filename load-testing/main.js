import { check, sleep } from "k6";
import http from "k6/http";

// script will simulate 10 virtual users (vus) making requests to the API endpoint concurrently for a duration of 30 seconds (duration).
// Test configuration
export let options = {
    vus: 10, // number of virtual users
    duration: "30s", // duration of the test
};

// user scenario
export default function () {
    // make an HTTP Get request
    let res = http.get(
        "http://a840213db1fa1454fadad370e4494e72-144729119.us-east-1.elb.amazonaws.com/api/metrics/octocat/Hello-World/code-review-involvement"
    );

    check(res, {
        "is status 200": (r) => r.status === 200,
    });

    // simulate user pacing by sleeping for a short duration
    sleep(5);
}
